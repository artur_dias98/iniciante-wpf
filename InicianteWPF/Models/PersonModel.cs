﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InicianteWPF.Models
{
    public class PersonModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
