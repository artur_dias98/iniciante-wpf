﻿using Caliburn.Micro;
using InicianteWPF.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InicianteWPF.ViewModels
{
    public class ShellViewModel : Conductor<object>
    {
        private InterTabClient interTabClient;
        private string _firstName;
        private string _lastName;
        private BindableCollection<PersonModel> _people = new BindableCollection<PersonModel>();
        private PersonModel _selectedPerson;
        public SecondChildViewModel SecondChildViewModel;
        public ShellViewModel()
        {
            People.Add(new PersonModel { FirstName = "Artur", LastName = "Dias" });
            People.Add(new PersonModel { FirstName = "Elma", LastName = "Maria" });
            People.Add(new PersonModel { FirstName = "Tomiei", LastName = "Nabuda" });
            People.Add(new PersonModel { FirstName = "Silas", LastName = "Kou" });
            InterTabClient = new InterTabClient();
            LoadPageTwo();
        }

        public InterTabClient InterTabClient
        {
            get
            {
                return interTabClient;
            }
            set
            {
                //if (interTabClient == value) return;
                interTabClient = value;
               
                NotifyOfPropertyChange(() => InterTabClient);
                
            }
        }
        //PROPRIEDADE QUE O NOSSO VIEW TEM ACESSO. DEVE-SE NOMEAR NO XAML IGUAL AO QUE ESTÁ NO VIEWMODEL
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                //NOTIFICAR PARA O VIEW QUE A PROPRIEDADE FOI MODIFICADA
                NotifyOfPropertyChange(() => FirstName);
                NotifyOfPropertyChange(() => FullName);
            }
        }          
        public string LastName
        {
            get 
            { 
                return _lastName; 
            }
            set 
            {
                _lastName = value;
                NotifyOfPropertyChange(() => LastName);
                NotifyOfPropertyChange(() => FullName);
            }
        }      
        //FULLNAME MODIFICA AUTOMATICAMENTO CONFORME FIRST E LASTNAME SÃO MODIFICADOS
        public string FullName
        {
            get 
            { 
                return $"{FirstName} {LastName}"; 
            }         
        }        
        //BINDABLECOLLECTION É COMO UM LISTA PARA O MVVM.
        public BindableCollection<PersonModel> People
        {
            get 
            {
                return _people; 
            }
            set 
            { 
                _people = value;
            }
        }
      
        public PersonModel SelectedPerson
        {
            get 
            { 
                return _selectedPerson; 
            }
            set 
            { 
                _selectedPerson = value;
                NotifyOfPropertyChange(() => SelectedPerson);
            }
        }
        public void Clear(string firstName, string lastName)
        {
            FirstName = "";
            LastName = "";
        }

        public bool CanClear(string firstName, string lastName)
        {
            return !String.IsNullOrWhiteSpace(firstName) && !String.IsNullOrWhiteSpace(lastName);
        }

        public void LoadPageOne()
        {
            ActivateItemAsync(new FirstChildViewModel());
        }

        public void LoadPageTwo()
        {
            ActivateItemAsync(new SecondChildViewModel());
        }

    }
}
